# C Wrapper for gRPC C++ Streaming Client

This is an example project for getting started with writing a C wrapper around gRPC C++ components. You will need `gRPC` and `make` installed on your system for this to work!

## Setup

```bash
# build the gRPC C++ Server and Client (ignore failure for `route_guide_client`, we only need `route_guide_client.o`)
./build_grpc_files.sh

# build the C Client
./build_c_client.sh
```

## Getting Started

```bash
# start the gRPC C++ Server
./route_guide_server

# run the C Client app
./c_client
```

Voila!
