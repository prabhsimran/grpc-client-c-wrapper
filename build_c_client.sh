#!/bin/bash

# builds the c_client manually
gcc -c c_client.c -o c_client.o

# links the grpc client to the c client
g++ c_client.o route_guide_client.o route_guide.pb.o helper.o route_guide.grpc.pb.o -L/usr/local/lib -lgrpc++ -lprotobuf -o c_client