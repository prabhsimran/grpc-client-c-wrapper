#include "route_guide_client.h"

extern "C" {

RouteGuideClient* newRouteGuideClient(char *msg) {
    std::string db = routeguide::GetDbFileContent();
    RouteGuideClient* client = new RouteGuideClient(
                                grpc::CreateChannel(
                                    "localhost:50051",
                                    grpc::InsecureChannelCredentials()
                                ), db);
    
    std::cout << "--------------" << msg << "--------------" << std::endl;
    return client;
}

int recordRoute(RouteGuideClient* client) {
    client->RecordRoute();
}

void deleteRouteGuideClient(RouteGuideClient* client) {
    delete client;
}

}