#ifndef __ROUTE_GUIDE_CLIENT_H
#define __ROUTE_GUIDE_CLIENT_H

#ifdef __cplusplus

#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <thread>

#include "helper.h"

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include "route_guide.grpc.pb.h"

class RouteGuideClient {

  public:
    RouteGuideClient(std::shared_ptr<grpc::Channel> channel, const std::string &db)
        : stub_(routeguide::RouteGuide::NewStub(channel)) {
        routeguide::ParseDb(db, &feature_list_);
    }

    void recordRouteInit(grpc::ClientWriter<routeguide::Point>* writer) {
        routeguide::Point point;
        routeguide::RouteSummary stats;
        grpc::ClientContext context;

        writer = new grpc::ClientWriter<routeguide::Point>(stub_->RecordRoute(&context, &stats));
    }

    void recordRouteProcess(grpc::ClientWriter<routeguide::Point>* writer, routeguide::Feature* f) {
        std::cout << "Visiting point "
                    << f->location().latitude() / kCoordFactor_ << ", "
                    << f->location().longitude() / kCoordFactor_ << std::endl;
        if (!writer->Write(f->location())) {
            // Broken stream.
            std::cout << "broken stream" << std::endl;
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    void recordRouteFinal(grpc::ClientWriter<routeguide::Point>* writer) {
        writer->WritesDone();
        grpc::Status status = writer->Finish();
        if (status.ok()) {
            std::cout << "Finished trip with " << stats.point_count() << " points\n"
                      << "Passed " << stats.feature_count() << " features\n"
                      << "Travelled " << stats.distance() << " meters\n"
                      << "It took " << stats.elapsed_time() << " seconds"
                      << std::endl;
        } else {
            std::cout << "RecordRoute rpc failed." << std::endl;
        }
    }

  private:

    const float kCoordFactor_ = 10000000.0;
    std::unique_ptr<routeguide::RouteGuide::Stub> stub_;
    std::vector<routeguide::Feature> feature_list_;
};

extern "C" {
#endif

typedef struct routeguide::Point Point;
typedef struct routeguide::Feature Feature;
typedef struct grpc::ClientWriter ClientWriter;
typedef struct RouteGuideClient RouteGuideClient;

Point* newPoint(int);
void deletePoint(Point*);

ClientWriter* newClientWriter(RouteGuideClient*);

RouteGuideClient* newRouteGuideClient(char *);
void deleteRouteGuideClient(RouteGuideClient*);
void recordRouteInit(RouteGuideClient*, ClientWriter*);
void recordRouteProcess(RouteGuideClient*, ClientWriter*, int);
void recordRouteFinal(RouteGuideClient*, ClientWriter*);


#ifdef __cplusplus
}
#endif

#endif