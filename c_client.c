#include <stdio.h>

#include "route_guide_client.h"

int main() {
    char str[] = "Calling grpc client rpc from C!";
    RouteGuideClient* client = newRouteGuideClient(str);
    recordRoute(client);
    deleteRouteGuideClient(client);
}